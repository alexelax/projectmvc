<%-- 
    Document   : professorinfo
    Created on : 9-dic-2018, 16.59.26
    Author     : Alex
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"/>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Informazioni Professori</title>
        <jsp:include page="header.jsp"/>
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><center><b><i>Informazioni Professore</i></b></center></div>
        <form action="./ProfessoriServlet" method="POST">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Campo</th>
                    </tr>
                </thead>
                <tbody>
                    <tr hidden="hidden">
                        <td>Professore ID</td>
                        <td><input class="form-control" type="hidden" name="id" value="${professore.id}" /></td>
                    </tr>
                    <tr>
                        <td>Nome</td>
                        <td><input class="form-control" type="text" name="nome" value="${professore.nome}" ${caricare==true?'required':''}/></td>
                    </tr>
                    <tr>
                        <td>Cognome</td>
                        <td><input class="form-control" type="text" name="cognome" value="${professore.cognome}" ${caricare==true?'required':''}/></td>
                    </tr>
                    <tr>
                        <td>Recapito</td>
                        <td><input class="form-control" type="text" name="recapito" value="${professore.recapito}" /></td>
                    </tr>               
                    <tr>
                        <td colspan="2">
                            <button class="btn btn-default btn-lg" type="submit" name="action" value="Add">
                                <i class="fas fa-plus-circle"></i>
                            </button>
                            <c:if test="${professore.id!=null}">
                                <button class="btn btn-default btn-lg" type="submit" name="action" value="Edit">
                                    <i class="fas fa-pen-nib"></i>
                                </button>
                            </c:if>
                            <input class="btn btn-default" style="display:none;" type="submit" name="action" value="Load" id="caricaPagina" />
                        </td>                
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Nome</th>
                <th scope="col">Cognome</th>
                <th scope="col">Recapito</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${allProfessori}" var="prof">
                <tr>
                    <td>${prof.id}</td>
                    <td>${prof.nome}</td>
                    <td>${prof.cognome}</td>
                    <td>${prof.recapito}</td>
                    <td>
                        <form action="./ProfessoriServlet" method="POST">
                            <input type="hidden" name="id" value="${prof.id}"/>
                            <button class="btn btn-default" type="submit" name="action" value="Delete">
                                <i class="fas fa-trash-alt"></i>
                            </button> 
                        </form>
                    </td>
                    <td>
                        <form action="./ProfessoriServlet" method="POST">
                            <input type="hidden" name="id" value="${prof.id}"/>
                            <button class="btn btn-default" type="submit" name="action" value="Load">
                                <i class="fas fa-upload"></i>
                            </button>
                        </form>
                    </td>
                </tr>
            </tbody>
        </c:forEach>
    </table> 
    <script>
        <c:if test="${caricare!=true}">
        $("#caricaPagina").trigger("click");
        </c:if>
    </script>
    <div class="panel-heading"><center><b><i>Cerca tramite campo: COGNOME</i></b></center>  
        <form action="./ProfessoriServlet" method="POST">
            <div class="row">
                <input type="text" name="campoRicerca" style="width:90%"/>
                <button class="btn btn-default" type="submit" name="action" value="Search">
                    <i class="fas fa-search"></i>
                </button> 
            </div>
        </form> 
    </div>
</div>



</body>
</html>





