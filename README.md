# Process and Development - Assignment 3 (MVC)

## Tasks
Develop an application that implements CRUD (create, read, update, delete) and search operations on a set of entities.  
There must be at least 4 entities in the system (e.g., worker, machine, site, shift), and at least 3 relations between those entities, including a self-relation.  
Arbitrarily define entities and attributes.  
The implementation must be in Java and based on a framework implementing JPA.  

The system consists of the following 4 entities: Courses - Exams - Professors - Students

Within these screens, 4 operations (CRUD) + 1 (search) have been implemented:

- Insert
- Delete
- Edit
- Load
- Search

The search operation can be "divided" into two parts:
- The first research is "global" and gives me all the results of the screen where I am
- The second research is based on a specific word; this last research is only present on the screens where it makes sense to be there

In the STUDENTS section the information of a student is displayed, with the possibility of assigning him a tutor.  
Exams passed by a student are displayed in the EXAMS section.  
In the PROFESSORS section the information of a professor is displayed.  
In the COURSES section the courses that can be visited are displayed.  

## Installation
For those who did not want to complete the project, but wanted directly the package .war jump to point 3.  
(The presence of java jdk version 8 is assumed for compilation and jre instead for execution only)  
- Install the latest available version of NetBeans (9.0)
- Import the project into netbeans
- Download and extract Glassfish 5
- Start glassfish
- Configure Connection Pool and JDBC resources (to avoid making changes to the project this should be called jdbc/StudentiDB) through the glassfish server configuration page. Instructions to configure it, are in the Assignment_README_2.pdf 
- The default configuration page can be found at: localhost: 4848
- Create an empty DB with StudentiDB as name and with this credentials: 
        user: alex password: 1234
- For those who use netbeans: it will be sufficient to launch the application, the DB will be created automatically before startup
- For those who do not use netbeans: it will be necessary to manually create tables and reports in the DB according to the ER-diagram in the Assignment_README_2.pdf and import the .war file in the deploy application section
- Start the browser 
- Open if you have not already done the browser at: http://localhost:8080/CRUDStudenti

That's it!
