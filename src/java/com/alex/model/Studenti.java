/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alex.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Alex
 */
@Entity
@Table(name = "STUDENTI")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Studenti.findAll", query = "SELECT s FROM Studenti s"),
    @NamedQuery(name = "Studenti.findByStudentiid", query = "SELECT s FROM Studenti s WHERE s.studentiid = :studentiid"),
    @NamedQuery(name = "Studenti.findByNome", query = "SELECT s FROM Studenti s WHERE s.nome = :nome"),
    @NamedQuery(name = "Studenti.findByCognome", query = "SELECT s FROM Studenti s WHERE s.cognome = :cognome"),
    @NamedQuery(name = "Studenti.findByMatricola", query = "SELECT s FROM Studenti s WHERE s.matricola = :matricola"),
    @NamedQuery(name = "Studenti.lastId", query = "SELECT MAX (s.studentiid) FROM Studenti s"),
    @NamedQuery(name = "Studenti.findByAnnodicorso", query = "SELECT s FROM Studenti s WHERE s.annodicorso = :annodicorso")})

public class Studenti implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "STUDENTIID")
    private Integer studentiid;
    @Size(max = 50)
    @Column(name = "NOME")
    private String nome;
    @Size(max = 50)
    @Column(name = "COGNOME")
    private String cognome;
    @Column(name = "ANNODICORSO")
    private Integer annodicorso;
    @Column(name = "MATRICOLA")
    private Integer matricola;
    @OneToMany(mappedBy = "idtutor")
    private Collection<Studenti> studentiCollection;
    @JoinColumn(name = "IDTUTOR", referencedColumnName = "STUDENTIID")
    @ManyToOne
    private Studenti idtutor;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idStudente")
    private Collection<Esami> esamiCollection;

    public Studenti() {
    }

    public Studenti(Integer studentiid) {
        this.studentiid = studentiid;
    }

    public Integer getStudentiid() {
        return studentiid;
    }

    public void setStudentiid(Integer studentiid) {
        this.studentiid = studentiid;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public Integer getAnnodicorso() {
        return annodicorso;
    }

    public void setAnnodicorso(Integer annodicorso) {
        this.annodicorso = annodicorso;
    }

    public Integer getMatricola() {
        return matricola;
    }

    public void setMatricola(Integer matricola) {
        this.matricola = matricola;
    }

    public void compilaCampi(String nome, String cognome, int matricola, int annodicorso) {
        this.nome = nome;
        this.cognome = cognome;
        this.matricola = matricola;
        this.annodicorso = annodicorso;
    }

    public boolean isTutor(int idTutor) {
        return idTutor == this.studentiid;
    }

    @XmlTransient
    public Collection<Studenti> getStudentiCollection() {
        return studentiCollection;
    }

    public void setStudentiCollection(Collection<Studenti> studentiCollection) {
        this.studentiCollection = studentiCollection;
    }

    public Studenti getIdtutor() {
        return idtutor;
    }

    public void setIdtutor(Studenti idtutor) {
        this.idtutor = idtutor;
    }

    @XmlTransient
    public Collection<Esami> getEsamiCollection() {
        return esamiCollection;
    }

    public void setEsamiCollection(Collection<Esami> esamiCollection) {
        this.esamiCollection = esamiCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (studentiid != null ? studentiid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Studenti)) {
            return false;
        }
        Studenti other = (Studenti) object;
        if ((this.studentiid == null && other.studentiid != null) || (this.studentiid != null && !this.studentiid.equals(other.studentiid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.alex.model.Studenti[ studentiid=" + studentiid + " ]";
    }

}
