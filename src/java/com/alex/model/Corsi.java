/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alex.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Alex
 */
@Entity
@Table(name = "CORSI")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Corsi.findAll", query = "SELECT c FROM Corsi c"),
    @NamedQuery(name = "Corsi.findById", query = "SELECT c FROM Corsi c WHERE c.id = :id"),
    @NamedQuery(name = "Corsi.findByNome", query = "SELECT c FROM Corsi c WHERE c.nome = :nome"),
    @NamedQuery(name = "Corsi.findByCfu", query = "SELECT c FROM Corsi c WHERE c.cfu = :cfu"),
    @NamedQuery(name = "Corsi.lastId", query = "SELECT MAX (c.id) FROM Corsi c"),
    @NamedQuery(name = "Corsi.findByDescrizione", query = "SELECT c FROM Corsi c WHERE c.descrizione = :descrizione")})
public class Corsi implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;
    @Size(max = 50)
    @Column(name = "NOME")
    private String nome;
    @Column(name = "CFU")
    private Integer cfu;
    @Size(max = 200)
    @Column(name = "DESCRIZIONE")
    private String descrizione;
    @JoinColumn(name = "ID_PROFESSORE", referencedColumnName = "ID")
    @ManyToOne
    private Professori idProfessore;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCorso")
    private Collection<Esami> esamiCollection;

    public Corsi() {
    }

    public Corsi(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getCfu() {
        return cfu;
    }

    public void setCfu(Integer cfu) {
        this.cfu = cfu;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public Professori getIdProfessore() {
        return idProfessore;
    }

    public void setIdProfessore(Professori idProfessore) {
        this.idProfessore = idProfessore;
    }

    public void compilaCampi(String nome, int cfu, String descrizione) {
        this.nome = nome;
        this.cfu = cfu;
        this.descrizione = descrizione;
    }

    @XmlTransient
    public Collection<Esami> getEsamiCollection() {
        return esamiCollection;
    }

    public void setEsamiCollection(Collection<Esami> esamiCollection) {
        this.esamiCollection = esamiCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Corsi)) {
            return false;
        }
        Corsi other = (Corsi) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.alex.model.Corsi[ id=" + id + " ]";
    }

}
