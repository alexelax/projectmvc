/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alex.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Alex
 */
@Entity
@Table(name = "PROFESSORI")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Professori.findAll", query = "SELECT p FROM Professori p"),
    @NamedQuery(name = "Professori.findById", query = "SELECT p FROM Professori p WHERE p.id = :id"),
    @NamedQuery(name = "Professori.findByNome", query = "SELECT p FROM Professori p WHERE p.nome = :nome"),
    @NamedQuery(name = "Professori.findByCognome", query = "SELECT p FROM Professori p WHERE p.cognome = :cognome"),
    @NamedQuery(name = "Professori.lastId", query = "SELECT MAX (p.id) FROM Professori p"),
    @NamedQuery(name = "Professori.findByRecapito", query = "SELECT p FROM Professori p WHERE p.recapito = :recapito")})
public class Professori implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOME")
    private String nome;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "COGNOME")
    private String cognome;
    @Size(max = 200)
    @Column(name = "RECAPITO")
    private String recapito;
    @OneToMany(mappedBy = "idProfessore")
    private Collection<Corsi> corsiCollection;

    public Professori() {
    }

    public Professori(Integer id) {
        this.id = id;
    }

    public Professori(Integer id, String nome, String cognome) {
        this.id = id;
        this.nome = nome;
        this.cognome = cognome;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getRecapito() {
        return recapito;
    }

    public void setRecapito(String recapito) {
        this.recapito = recapito;
    }

    public void compilaCampi(String nome, String cognome, String recapito) {
        this.nome = nome;
        this.cognome = cognome;
        this.recapito = recapito;
    }

    @XmlTransient
    public Collection<Corsi> getCorsiCollection() {
        return corsiCollection;
    }

    public void setCorsiCollection(Collection<Corsi> corsiCollection) {
        this.corsiCollection = corsiCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Professori)) {
            return false;
        }
        Professori other = (Professori) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.alex.model.Professori[ id=" + id + " ]";
    }

}
