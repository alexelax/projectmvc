/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alex.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Alex
 */
@Entity
@Table(name = "ESAMI")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Esami.findAll", query = "SELECT e FROM Esami e"),
    @NamedQuery(name = "Esami.findById", query = "SELECT e FROM Esami e WHERE e.id = :id"),
    @NamedQuery(name = "Esami.findByVoto", query = "SELECT e FROM Esami e WHERE e.voto = :voto"),
    @NamedQuery(name = "Esami.lastId", query = "SELECT MAX (e.id) FROM Esami e"),
    @NamedQuery(name = "Esami.findByAnno", query = "SELECT e FROM Esami e WHERE e.anno = :anno")})
public class Esami implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Min(18)
    @Max(31)
    @Column(name = "VOTO")
    private int voto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ANNO")
    private int anno;
    @JoinColumn(name = "ID_CORSO", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Corsi idCorso;
    @JoinColumn(name = "ID_STUDENTE", referencedColumnName = "STUDENTIID")
    @ManyToOne(optional = false)
    private Studenti idStudente;

    public Esami() {
    }

    public Esami(Integer id) {
        this.id = id;
    }

    public Esami(Integer id, int voto, int anno) {
        this.id = id;
        this.voto = voto;
        this.anno = anno;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getVoto() {
        return voto;
    }

    public void setVoto(int voto) {
        this.voto = voto;
    }

    public int getAnno() {
        return anno;
    }

    public void setAnno(int anno) {
        this.anno = anno;
    }

    public Corsi getIdCorso() {
        return idCorso;
    }

    public void setIdCorso(Corsi idCorso) {
        this.idCorso = idCorso;
    }

    public Studenti getIdStudente() {
        return idStudente;
    }

    public void setIdStudente(Studenti idStudente) {
        this.idStudente = idStudente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Esami)) {
            return false;
        }
        Esami other = (Esami) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.alex.model.Esami[ id=" + id + " ]";
    }

}
