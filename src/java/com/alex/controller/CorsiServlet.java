/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alex.controller;

import com.alex.dao.CorsiDaoLocal;
import com.alex.dao.ProfessoriDaoLocal;
import com.alex.model.Corsi;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Alex
 */
@WebServlet(name = "CorsiServlet", urlPatterns = {"/CorsiServlet"})
public class CorsiServlet extends HttpServlet {

    @EJB
    private CorsiDaoLocal corsiDao;
    @EJB
    private ProfessoriDaoLocal professoriDao;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        request.setAttribute("pagina", "corsi");
        int corsiId = 0;
        String corsiIdStr = request.getParameter("id");
        if (corsiIdStr != null && !corsiIdStr.equals("")) {
            corsiId = Integer.parseInt(corsiIdStr);
        }
        int profid = 0;
        String prof = request.getParameter("idProfessore");
        if (prof != null && !prof.equals("")) {
            profid = Integer.parseInt(prof);
        }
        String nome = request.getParameter("nome");
        int cfu = 0;
        String cfuStr = request.getParameter("cfu");
        if (cfuStr != null && !cfuStr.equals("")) {
            cfu = Integer.parseInt(cfuStr);
        }
        String descrizione = request.getParameter("descrizione");

        Corsi corso = new Corsi(corsiId);
        corso.compilaCampi(nome, cfu, descrizione);
        corso.setIdProfessore(professoriDao.getProfessore(profid));

        if ("Add".equalsIgnoreCase(action)) {
            corsiDao.addCorso(corso);
        } else if ("Edit".equalsIgnoreCase(action)) {
            corsiDao.editCorso(corso);
        } else if ("Delete".equalsIgnoreCase(action)) {
            corsiDao.deleteCorso(corsiId);
        } else if ("Load".equalsIgnoreCase(action) && StaticVariables.caricato != true) {
            StaticVariables.caricato = true;
            request.setAttribute("caricare", true);
            corso = corsiDao.getCorso(corsiId);
        } else if ("Search".equalsIgnoreCase(action)) {
            corso = corsiDao.getCorsiByNome(request.getParameter("campoRicerca"));
        }

        StaticVariables.caricato = false;
        request.setAttribute("corso", corso);
        if ("Search".equalsIgnoreCase(action) && StaticVariables.search != true) {
            List<Corsi> cors = new ArrayList<Corsi>();
            if (corso != null && corso.getId() > 0) {
                cors.add(corso);
            }
            StaticVariables.search = true;
            request.setAttribute("caricare", true);
            request.setAttribute("allCorsi", cors);
        } else {
            request.setAttribute("allCorsi", corsiDao.getAllCorsi());
        }

        StaticVariables.search = false;
        request.setAttribute("allProfessori", professoriDao.getAllProfessori());
        request.getRequestDispatcher("corsinfo.jsp").forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
