/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alex.controller;

import com.alex.dao.StudentiDaoLocal;
import com.alex.model.Studenti;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Alex
 */
@WebServlet(name = "StudentiServlet", urlPatterns = {"/StudentiServlet"})
public class StudentiServlet extends HttpServlet {

    @EJB
    private StudentiDaoLocal studentiDao;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        request.setAttribute("pagina", "studenti");
        int studenteId = 0;
        String studentiIdStr = request.getParameter("studentiid");
        if (studentiIdStr != null && !studentiIdStr.equals("")) {
            studenteId = Integer.parseInt(studentiIdStr);
        }

        String tutor = request.getParameter("idtutor");
        int tutorid = 0;
        if (tutor != null && !tutor.equals("")) {
            tutorid = Integer.parseInt(tutor);
        }
        String nome = request.getParameter("nome");
        String cognome = request.getParameter("cognome");
        String matrStr = request.getParameter("matricola");
        int matricola = 0;
        if (matrStr != null && !matrStr.equals("")) {
            matricola = Integer.parseInt(matrStr);
        }
        String annodicorsoStr = request.getParameter("annocorso");
        int annodicorso = 0;
        if (annodicorsoStr != null && !annodicorsoStr.equals("")) {
            annodicorso = Integer.parseInt(annodicorsoStr);
        }

        Studenti studente = new Studenti(studenteId);
        studente.compilaCampi(nome, cognome, matricola, annodicorso);
        studente.setIdtutor(studentiDao.getStudente(tutorid));

        if ("Add".equalsIgnoreCase(action)) {
            studentiDao.addStudente(studente);
        } else if ("Edit".equalsIgnoreCase(action)) {
            studentiDao.editStudente(studente);
        } else if ("Delete".equalsIgnoreCase(action)) {
            studentiDao.deleteStudente(studenteId);
        } else if ("Load".equalsIgnoreCase(action) && StaticVariables.caricato != true) {
            StaticVariables.caricato = true;
            request.setAttribute("caricare", true);
            studente = studentiDao.getStudente(studenteId);
        } else if ("Search".equalsIgnoreCase(action)) {
            studente = studentiDao.getStudenteByMatricola(Integer.parseInt(request.getParameter("campoRicerca")));
        }

        StaticVariables.caricato = false;
        request.setAttribute("studente", studente);
        if ("Search".equalsIgnoreCase(action) && StaticVariables.search != true) {
            List<Studenti> stud = new ArrayList<Studenti>();
            if (studente != null && studente.getStudentiid() > 0) {
                stud.add(studente);
            }
            StaticVariables.search = true;
            request.setAttribute("caricare", true);
            request.setAttribute("allStudenti", stud);
        } else {
            request.setAttribute("allStudenti", studentiDao.getAllStudenti());
        }

        StaticVariables.search = false;
        request.getRequestDispatcher("studentinfo.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
