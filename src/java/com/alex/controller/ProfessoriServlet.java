/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alex.controller;

import com.alex.dao.ProfessoriDaoLocal;
import com.alex.model.Professori;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Alex
 */
@WebServlet(name = "ProfessoriServlet", urlPatterns = {"/ProfessoriServlet"})
public class ProfessoriServlet extends HttpServlet {

    @EJB
    private ProfessoriDaoLocal ProfessoriDao;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        request.setAttribute("pagina", "professori");
        int profId = 0;
        String profIdStr = request.getParameter("id");
        if (profIdStr != null && !profIdStr.equals("")) {
            profId = Integer.parseInt(profIdStr);
        }
        String nome = request.getParameter("nome");
        String cognome = request.getParameter("cognome");
        String recapito = request.getParameter("recapito");

        Professori professore = new Professori(profId);
        professore.compilaCampi(nome, cognome, recapito);

        if ("Add".equalsIgnoreCase(action)) {
            ProfessoriDao.addProfessore(professore);
        } else if ("Edit".equalsIgnoreCase(action)) {
            ProfessoriDao.editProfessore(professore);
        } else if ("Delete".equalsIgnoreCase(action)) {
            ProfessoriDao.deleteProfessore(profId);
        } else if ("Load".equalsIgnoreCase(action) && StaticVariables.caricato != true) {
            StaticVariables.caricato = true;
            request.setAttribute("caricare", true);
            professore = ProfessoriDao.getProfessore(profId);
        } else if ("Search".equalsIgnoreCase(action)) {
            professore = ProfessoriDao.getProfessoreByCognome(request.getParameter("campoRicerca"));
        }

        StaticVariables.caricato = false;
        request.setAttribute("professore", professore);
        if ("Search".equalsIgnoreCase(action) && StaticVariables.search != true) {
            List<Professori> prof = new ArrayList<Professori>();
            if (professore != null && professore.getId() > 0) {
                prof.add(professore);
            }
            StaticVariables.search = true;
            request.setAttribute("caricare", true);
            request.setAttribute("allProfessori", prof);
        } else {
            request.setAttribute("allProfessori", ProfessoriDao.getAllProfessori());
        }

        StaticVariables.search = false;
        request.getRequestDispatcher("professorinfo.jsp").forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
