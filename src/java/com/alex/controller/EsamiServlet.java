/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alex.controller;

import com.alex.dao.CorsiDaoLocal;
import com.alex.dao.EsamiDaoLocal;
import com.alex.dao.StudentiDaoLocal;
import com.alex.model.Corsi;
import com.alex.model.Esami;
import com.alex.model.Studenti;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Alex
 */
@WebServlet(name = "EsamiServlet", urlPatterns = {"/EsamiServlet"})
public class EsamiServlet extends HttpServlet {

    @EJB
    private EsamiDaoLocal esamiDao;
    @EJB
    private CorsiDaoLocal corsiDao;
    @EJB
    private StudentiDaoLocal studentiDao;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        request.setAttribute("pagina", "esami");
        int esamiId = 0;
        String esamiIdStr = request.getParameter("id");
        if (esamiIdStr != null && !esamiIdStr.equals("")) {
            esamiId = Integer.parseInt(esamiIdStr);
        }

        int voto = 0;
        String votoStr = request.getParameter("voto");
        if (votoStr != null && !votoStr.equals("")) {
            voto = Integer.parseInt(votoStr);
        }

        int anno = 0;
        String annoStr = request.getParameter("anno");
        if (annoStr != null && !annoStr.equals("")) {
            anno = Integer.parseInt(annoStr);
        }
        Studenti studente = null;
        String idStudenteSTR = request.getParameter("idStudente");
        if (idStudenteSTR != null && !idStudenteSTR.equals("")) {
            studente = studentiDao.getStudente(Integer.parseInt(idStudenteSTR));
        }
        Corsi corso = null;
        String idCorsoSTR = request.getParameter("idCorso");
        if (idCorsoSTR != null && !idCorsoSTR.equals("")) {
            corso = corsiDao.getCorso(Integer.parseInt(idCorsoSTR));
        }
        Esami esame = new Esami(esamiId);
        esame.setAnno(anno);
        esame.setVoto(voto);
        esame.setIdStudente(studente);
        esame.setIdCorso(corso);
        if ("Add".equalsIgnoreCase(action)) {
            esamiDao.addEsame(esame);
        } else if ("Edit".equalsIgnoreCase(action)) {
            esamiDao.editEsame(esame);
        } else if ("Delete".equalsIgnoreCase(action)) {
            esamiDao.deleteEsame(esamiId);
        } else if ("Load".equalsIgnoreCase(action) && StaticVariables.caricato != true) {
            StaticVariables.caricato = true;
            request.setAttribute("caricare", true);
            esame = esamiDao.getEsame(esamiId);
        }

        StaticVariables.caricato = false;
        request.setAttribute("esame", esame);
        request.setAttribute("allEsami", esamiDao.getAllEsami());
        request.setAttribute("allStudenti", studentiDao.getAllStudenti());
        request.setAttribute("allCorsi", corsiDao.getAllCorsi());
        request.getRequestDispatcher("esaminfo.jsp").forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
