/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alex.dao;

import com.alex.model.Professori;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Alex
 */
@Stateless
public class ProfessoriDao implements ProfessoriDaoLocal {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void addProfessore(Professori professore) {
        em.persist(professore);
    }

    @Override
    public void editProfessore(Professori professore) {
        em.merge(professore);
    }

    @Override
    public void deleteProfessore(int professoreId) {
        em.remove(getProfessore(professoreId));
    }

    @Override
    public Professori getProfessore(int professoreId) {
        return em.find(Professori.class, professoreId);
    }

    @Override
    public List<Professori> getAllProfessori() {
        return em.createNamedQuery("Professori.findAll").getResultList();
    }

    @Override
    public int getId() {
        return ((int) em.createNamedQuery("Professori.lastId").getResultList().get(0)) + 1;
    }

    @Override
    public Professori getProfessoreByCognome(String cognome) {
        try {
            return em.createNamedQuery("Professori.findByCognome", Professori.class).setParameter("cognome", cognome).getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

}
