/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alex.dao;

import com.alex.model.Corsi;
import java.util.List;

/**
 *
 * @author Alex
 */
public interface CorsiDaoLocal {

    void addCorso(Corsi corso);

    void editCorso(Corsi corso);

    void deleteCorso(int corsoId);

    Corsi getCorso(int corsoId);

    List<Corsi> getAllCorsi();

    int getId();

    Corsi getCorsiByNome(String nome);
}
