/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alex.dao;

import com.alex.model.Studenti;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Alex
 */
@Local
public interface StudentiDaoLocal {

    void addStudente(Studenti studente);

    void editStudente(Studenti studente);

    void deleteStudente(int studentiid);

    Studenti getStudente(int studentiid);

    List<Studenti> getAllStudenti();

    int getId();

    Studenti getStudenteByMatricola(int matricola);

}
