/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alex.dao;

import com.alex.model.Corsi;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Alex
 */
@Stateless
public class CorsiDao implements CorsiDaoLocal {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void addCorso(Corsi corso) {
        em.persist(corso);
    }

    @Override
    public void editCorso(Corsi corso) {
        em.merge(corso);
    }

    @Override
    public void deleteCorso(int corsoId) {
        em.remove(getCorso(corsoId));
    }

    @Override
    public Corsi getCorso(int corsoId) {
        return em.find(Corsi.class, corsoId);
    }

    @Override
    public List<Corsi> getAllCorsi() {
        return em.createNamedQuery("Corsi.findAll").getResultList();
    }

    @Override
    public int getId() {
        return ((int) em.createNamedQuery("Corsi.lastId").getResultList().get(0)) + 1;
    }

    @Override
    public Corsi getCorsiByNome(String nome) {
        try {
            return em.createNamedQuery("Corsi.findByNome", Corsi.class).setParameter("nome", nome).getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

}
