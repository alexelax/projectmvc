/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alex.dao;

import com.alex.model.Esami;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Alex
 */
@Local
public interface EsamiDaoLocal {

    void addEsame(Esami esame);

    void editEsame(Esami esame);

    void deleteEsame(int esameId);

    Esami getEsame(int esameId);

    List<Esami> getAllEsami();

    int getId();

}
