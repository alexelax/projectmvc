/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alex.dao;

import com.alex.model.Professori;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Alex
 */
@Local
public interface ProfessoriDaoLocal {

    void addProfessore(Professori professore);

    void editProfessore(Professori professore);

    void deleteProfessore(int professoreId);

    Professori getProfessore(int professoreId);

    List<Professori> getAllProfessori();

    int getId();
    Professori getProfessoreByCognome(String cognome);
}
