/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alex.dao;

import com.alex.model.Studenti;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Alex
 */
@Stateless
public class StudentiDao implements StudentiDaoLocal {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void addStudente(Studenti studente) {
        em.persist(studente);
    }

    @Override
    public void editStudente(Studenti studente) {
        em.merge(studente);
    }

    @Override
    public void deleteStudente(int studentiid) {
        em.remove(getStudente(studentiid));
    }

    @Override
    public Studenti getStudente(int studentiid) {
        return em.find(Studenti.class, studentiid);
    }

    @Override
    public List<Studenti> getAllStudenti() {
        return em.createNamedQuery("Studenti.findAll").getResultList();
    }

    @Override
    public int getId() {
        return ((int) em.createNamedQuery("Studenti.lastId").getResultList().get(0)) + 1;
    }

    @Override
    public Studenti getStudenteByMatricola(int matricola) {
        try {
            return em.createNamedQuery("Studenti.findByMatricola", Studenti.class).setParameter("matricola", matricola).getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

}
