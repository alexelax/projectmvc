/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alex.dao;

import com.alex.model.Esami;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Alex
 */
@Stateless
public class EsamiDao implements EsamiDaoLocal {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void addEsame(Esami esame) {
        em.persist(esame);
    }

    @Override
    public void editEsame(Esami esame) {
        em.merge(esame);
    }

    @Override
    public void deleteEsame(int esameId) {
        em.remove(getEsame(esameId));
    }

    @Override
    public Esami getEsame(int esameId) {
        return em.find(Esami.class, esameId);
    }

    @Override
    public List<Esami> getAllEsami() {
        return em.createNamedQuery("Esami.findAll").getResultList();
    }

    @Override
    public int getId() {
        return ((int) em.createNamedQuery("Esami.lastId").getResultList().get(0)) + 1;
    }

}
