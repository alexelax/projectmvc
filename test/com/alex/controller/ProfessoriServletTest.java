package com.alex.controller;

import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class ProfessoriServletTest {

    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {
        System.setProperty("webdriver.gecko.driver", "c:\\Temp\\geckodriver.exe");

        //Now you can initialize marionette driver to launch firefox
        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        capabilities.setCapability("marionette", true);
        driver = new FirefoxDriver();
        baseUrl = "http://localhost:8080/CRUDStudenti/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @Test
    public void testProfessori() throws Exception {
        driver.get(baseUrl);
        driver.findElement(By.linkText("Professori")).click();
        driver.findElement(By.name("nome")).click();
        driver.findElement(By.name("nome")).clear();
        driver.findElement(By.name("nome")).sendKeys("Leonardo");
        driver.findElement(By.name("cognome")).clear();
        driver.findElement(By.name("cognome")).sendKeys("Mariani");
        driver.findElement(By.name("recapito")).clear();
        driver.findElement(By.name("recapito")).sendKeys("l.mariani@disco.it");
        driver.findElement(By.name("action")).click();
        driver.findElement(By.name("nome")).click();
        driver.findElement(By.name("nome")).clear();
        driver.findElement(By.name("nome")).sendKeys("Giancarlo");
        driver.findElement(By.name("cognome")).clear();
        driver.findElement(By.name("cognome")).sendKeys("Mauri");
        driver.findElement(By.name("recapito")).clear();
        driver.findElement(By.name("recapito")).sendKeys("1234");
        driver.findElement(By.name("action")).click();
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='l.mariani@disco.it'])[1]/following::button[2]")).click();
        driver.findElement(By.name("recapito")).click();
        driver.findElement(By.name("recapito")).clear();
        driver.findElement(By.name("recapito")).sendKeys("l.mariani@disco.unimib.it");
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Recapito'])[1]/following::button[2]")).click();
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Mauri'])[1]/following::button[1]")).click();
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='l.mariani@disco.unimib.it'])[1]/following::button[1]")).click();
        driver.findElement(By.name("nome")).click();
        driver.findElement(By.name("nome")).clear();
        driver.findElement(By.name("nome")).sendKeys("Leonardo");
        driver.findElement(By.name("cognome")).clear();
        driver.findElement(By.name("cognome")).sendKeys("Mariani");
        driver.findElement(By.name("recapito")).clear();
        driver.findElement(By.name("recapito")).sendKeys("l.mariani@disco.it");
        driver.findElement(By.name("action")).click();
        driver.findElement(By.name("nome")).click();
        driver.findElement(By.name("nome")).clear();
        driver.findElement(By.name("nome")).sendKeys("Giancarlo");
        driver.findElement(By.name("cognome")).clear();
        driver.findElement(By.name("cognome")).sendKeys("Mauri");
        driver.findElement(By.name("recapito")).clear();
        driver.findElement(By.name("recapito")).sendKeys("1234");
        driver.findElement(By.name("action")).click();
        driver.findElement(By.name("campoRicerca")).click();
        driver.findElement(By.name("campoRicerca")).clear();
        driver.findElement(By.name("campoRicerca")).sendKeys("Mariani");
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Cerca tramite campo: COGNOME'])[1]/following::button[1]")).click();
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
