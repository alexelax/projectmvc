package com.alex.controller;

import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;

public class EsamiServletTest {

    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {
        System.setProperty("webdriver.gecko.driver", "c:\\Temp\\geckodriver.exe");

        //Now you can initialize marionette driver to launch firefox
        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        capabilities.setCapability("marionette", true);
        driver = new FirefoxDriver();
        baseUrl = "http://localhost:8080/CRUDStudenti/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @Test
    public void testEsami() throws Exception {
        driver.get(baseUrl);
        driver.findElement(By.linkText("Esami")).click();
        driver.findElement(By.name("idStudente")).click();
        driver.findElement(By.name("idStudente")).click();
        driver.findElement(By.name("idCorso")).click();
        driver.findElement(By.name("idCorso")).click();
        driver.findElement(By.linkText("Corsi")).click();
        driver.findElement(By.name("idProfessore")).click();
        driver.findElement(By.name("idProfessore")).click();
        driver.findElement(By.linkText("Professori")).click();
        driver.findElement(By.name("nome")).click();
        driver.findElement(By.name("nome")).clear();
        driver.findElement(By.name("nome")).sendKeys("Leonardo");
        driver.findElement(By.name("cognome")).clear();
        driver.findElement(By.name("cognome")).sendKeys("Mariani");
        driver.findElement(By.name("recapito")).clear();
        driver.findElement(By.name("recapito")).sendKeys("l.mariani@disco.unimib.it");
        driver.findElement(By.name("action")).click();
        driver.findElement(By.linkText("Corsi")).click();
        driver.findElement(By.name("nome")).click();
        driver.findElement(By.name("nome")).clear();
        driver.findElement(By.name("nome")).sendKeys("Processo e Sviluppo Software");
        driver.findElement(By.name("cfu")).clear();
        driver.findElement(By.name("cfu")).sendKeys("6");
        driver.findElement(By.name("idProfessore")).click();
        new Select(driver.findElement(By.name("idProfessore"))).selectByVisibleText("Leonardo Mariani");
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Professore'])[1]/following::option[2]")).click();
        driver.findElement(By.name("action")).click();
        driver.findElement(By.linkText("Studenti")).click();
        driver.findElement(By.name("nome")).click();
        driver.findElement(By.name("nome")).clear();
        driver.findElement(By.name("nome")).sendKeys("Alessandro");
        driver.findElement(By.name("cognome")).clear();
        driver.findElement(By.name("cognome")).sendKeys("Vazzola");
        driver.findElement(By.name("matricola")).clear();
        driver.findElement(By.name("matricola")).sendKeys("781061");
        driver.findElement(By.name("annocorso")).clear();
        driver.findElement(By.name("annocorso")).sendKeys("2018");
        driver.findElement(By.name("action")).click();
        driver.findElement(By.linkText("Esami")).click();
        driver.findElement(By.name("idStudente")).click();
        new Select(driver.findElement(By.name("idStudente"))).selectByVisibleText("Alessandro Vazzola");
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Studente'])[1]/following::option[2]")).click();
        driver.findElement(By.name("idCorso")).click();
        new Select(driver.findElement(By.name("idCorso"))).selectByVisibleText("Processo e Sviluppo Software");
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Corso'])[1]/following::option[2]")).click();
        driver.findElement(By.name("voto")).click();
        driver.findElement(By.name("voto")).clear();
        driver.findElement(By.name("voto")).sendKeys("31");
        driver.findElement(By.name("anno")).click();
        driver.findElement(By.name("anno")).clear();
        driver.findElement(By.name("anno")).sendKeys("2018");
        driver.findElement(By.name("action")).click();
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Processo e Sviluppo Software'])[2]/following::button[2]")).click();
        driver.findElement(By.name("voto")).click();
        driver.findElement(By.name("voto")).clear();
        driver.findElement(By.name("voto")).sendKeys("30");
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Corso'])[1]/following::button[2]")).click();
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Processo e Sviluppo Software'])[2]/following::button[1]")).click();
        driver.findElement(By.linkText("Studenti")).click();
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Vazzola'])[1]/following::button[1]")).click();
        driver.findElement(By.linkText("Corsi")).click();
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Mariani'])[1]/following::button[1]")).click();
        driver.findElement(By.linkText("Professori")).click();
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='l.mariani@disco.unimib.it'])[1]/following::button[1]")).click();
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
