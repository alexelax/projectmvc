/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alex.DAO;

import com.alex.dao.StudentiDaoLocal;
import com.alex.model.Studenti;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.embeddable.EJBContainer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Alex
 */
public class StudentiDaoTest {

    EJBContainer container;

    public StudentiDaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        Map<String, Object> properties = new HashMap<String, Object>();
        properties.put(EJBContainer.MODULES, new File("build\\web\\WEB-INF\\classes"));
        container = javax.ejb.embeddable.EJBContainer.createEJBContainer(properties);
    }

    @After
    public void tearDown() {
        container.close();
    }

    /**
     * Test of addStudente method, of class StudentiDao.
     */
    @Test
    public void testAddStudente() throws Exception {
        System.out.println("addStudente");
        Studenti studente = new Studenti();
        studente.setCognome("cognome");
        studente.setNome("nome");
        studente.setMatricola(781061);
        studente.setAnnodicorso(2018);
        studente.setStudentiid(0);
        Object cc = container.getContext().lookup("java:global/classes/StudentiDao");
        StudentiDaoLocal s = (StudentiDaoLocal) cc;
        s.addStudente(studente);
        s.deleteStudente(1);
    }

    /**
     * Test of editStudente method, of class StudentiDao.
     */
    @Test
    public void testEditStudente() throws Exception {
        System.out.println("editStudente");
        Studenti studente = new Studenti();
        studente.setCognome("cognome");
        studente.setNome("nome");
        studente.setMatricola(781061);
        studente.setAnnodicorso(2018);
        studente.setStudentiid(0);
        Object cc = container.getContext().lookup("java:global/classes/StudentiDao");
        StudentiDaoLocal s = (StudentiDaoLocal) cc;
        s.addStudente(studente);
        Studenti result = s.getStudente(1);
        result.setCognome("edit");
        s.editStudente(result);
        s.deleteStudente(1);
    }

    /**
     * Test of findStudente method, of class StudentiDao.
     */
    @Test
    public void testFindStudente() throws Exception {
        System.out.println("findStudente");
        Studenti studente = new Studenti();
        studente.setCognome("cognome");
        studente.setNome("nome");
        studente.setMatricola(781061);
        studente.setAnnodicorso(2018);
        studente.setStudentiid(0);
        Object cc = container.getContext().lookup("java:global/classes/StudentiDao");
        StudentiDaoLocal s = (StudentiDaoLocal) cc;
        s.addStudente(studente);
        s.getStudenteByMatricola(781061);
        s.deleteStudente(1);
    }

    /**
     * Test of deleteStudente method, of class StudentiDao.
     */
    @Test
    public void testDeleteStudente() throws Exception {
        System.out.println("deleteStudente");
        Studenti studente = new Studenti();
        studente.setCognome("cognome");
        studente.setNome("nome");
        studente.setMatricola(781061);
        studente.setAnnodicorso(2018);
        studente.setStudentiid(0);
        Object cc = container.getContext().lookup("java:global/classes/StudentiDao");
        StudentiDaoLocal s = (StudentiDaoLocal) cc;
        s.addStudente(studente);
        s.deleteStudente(1);
    }
}
