/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alex.DAO;

import com.alex.dao.ProfessoriDaoLocal;
import com.alex.model.Professori;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.embeddable.EJBContainer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Alex
 */
public class ProfessoriDaoTest {

    EJBContainer container;

    public ProfessoriDaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        Map<String, Object> properties = new HashMap<String, Object>();
        properties.put(EJBContainer.MODULES, new File("build\\web\\WEB-INF\\classes"));
        container = javax.ejb.embeddable.EJBContainer.createEJBContainer(properties);
    }

    @After
    public void tearDown() {
        container.close();
    }

    /**
     * Test of addProfessore method, of class ProfessoriDao.
     */
    @Test
    public void testAddProfessore() throws Exception {
        System.out.println("addProfessore");
        Professori professore = new Professori();
        professore.setCognome("cognome");
        professore.setNome("nome");
        professore.setRecapito("abc");
        professore.setId(0);
        Object cc = container.getContext().lookup("java:global/classes/ProfessoriDao");
        ProfessoriDaoLocal p = (ProfessoriDaoLocal) cc;
        p.addProfessore(professore);
        p.deleteProfessore(1);
    }

    /**
     * Test of editProfessore method, of class ProfessoriDao.
     */
    @Test
    public void testEditProfessore() throws Exception {
        System.out.println("editProfessore");
        Professori professore = new Professori();
        professore.setCognome("cognome");
        professore.setNome("nome");
        professore.setRecapito("abc");
        professore.setId(0);
        Object cc = container.getContext().lookup("java:global/classes/ProfessoriDao");
        ProfessoriDaoLocal p = (ProfessoriDaoLocal) cc;
        p.addProfessore(professore);
        Professori result = p.getProfessore(1);
        result.setCognome("edit");
        p.editProfessore(result);
        p.deleteProfessore(1);
    }

    /**
     * Test of findProfessore method, of class ProfessoriDao.
     */
    @Test
    public void testFindProfessore() throws Exception {
        System.out.println("findProfessore");
        Professori professore = new Professori();
        professore.setCognome("cognome");
        professore.setNome("nome");
        professore.setRecapito("abc");
        professore.setId(0);
        Object cc = container.getContext().lookup("java:global/classes/ProfessoriDao");
        ProfessoriDaoLocal p = (ProfessoriDaoLocal) cc;
        p.addProfessore(professore);
        p.getProfessoreByCognome("cognome");
        p.deleteProfessore(1);
    }

    /**
     * Test of deleteProfessore method, of class ProfessoriDao.
     */
    @Test
    public void testDeleteProfessore() throws Exception {
        System.out.println("deleteProfessore");
        Professori professore = new Professori();
        professore.setCognome("cognome");
        professore.setNome("nome");
        professore.setRecapito("abc");
        professore.setId(0);
        Object cc = container.getContext().lookup("java:global/classes/ProfessoriDao");
        ProfessoriDaoLocal p = (ProfessoriDaoLocal) cc;
        p.addProfessore(professore);
        p.deleteProfessore(1);
    }
}
