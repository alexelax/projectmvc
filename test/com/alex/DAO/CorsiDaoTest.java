/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alex.DAO;

import com.alex.dao.ProfessoriDaoLocal;
import com.alex.model.Professori;
import com.alex.dao.CorsiDaoLocal;
import com.alex.model.Corsi;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.embeddable.EJBContainer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Alex
 */
public class CorsiDaoTest {

    EJBContainer container;

    public CorsiDaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        Map<String, Object> properties = new HashMap<String, Object>();
        properties.put(EJBContainer.MODULES, new File("build\\web\\WEB-INF\\classes"));
        container = javax.ejb.embeddable.EJBContainer.createEJBContainer(properties);
    }

    @After
    public void tearDown() {
        container.close();
    }

    /**
     * Test of addCorso method, of class CorsiDao.
     */
    @Test
    public void testAddCorso() throws Exception {
        System.out.println("addCorso");
        Professori professore = new Professori();
        professore.setCognome("cognome");
        professore.setNome("nome");
        professore.setRecapito("abc");
        professore.setId(0);
        Object pp = container.getContext().lookup("java:global/classes/ProfessoriDao");
        ProfessoriDaoLocal p = (ProfessoriDaoLocal) pp;
        p.addProfessore(professore);
        p.getProfessore(1);

        Corsi corso = new Corsi();
        corso.setNome("nome");
        corso.setCfu(6);
        corso.setDescrizione("abc");
        corso.setIdProfessore(professore);
        corso.setId(0);
        Object cc = container.getContext().lookup("java:global/classes/CorsiDao");
        CorsiDaoLocal c = (CorsiDaoLocal) cc;
        c.addCorso(corso);
        c.deleteCorso(1);
        p.deleteProfessore(1);
    }

    /**
     * Test of editCorso method, of class CorsiDao.
     */
    @Test
    public void testEditCorso() throws Exception {
        System.out.println("editCorso");
        Professori professore = new Professori();
        professore.setCognome("cognome");
        professore.setNome("nome");
        professore.setRecapito("abc");
        professore.setId(0);
        Object pp = container.getContext().lookup("java:global/classes/ProfessoriDao");
        ProfessoriDaoLocal p = (ProfessoriDaoLocal) pp;
        p.addProfessore(professore);
        p.getProfessore(1);

        Corsi corso = new Corsi();
        corso.setNome("nome");
        corso.setCfu(6);
        corso.setDescrizione("abc");
        corso.setIdProfessore(professore);
        corso.setId(0);
        Object cc = container.getContext().lookup("java:global/classes/CorsiDao");
        CorsiDaoLocal c = (CorsiDaoLocal) cc;
        c.addCorso(corso);
        Professori result = p.getProfessore(1);
        result.setCognome("edit");
        p.editProfessore(result);
        c.deleteCorso(1);
        p.deleteProfessore(1);
    }

    /**
     * Test of findCorso method, of class CorsiDao.
     */
    @Test
    public void testFindCorso() throws Exception {
        System.out.println("findCorso");
        Professori professore = new Professori();
        professore.setCognome("cognome");
        professore.setNome("nome");
        professore.setRecapito("abc");
        professore.setId(0);
        Object pp = container.getContext().lookup("java:global/classes/ProfessoriDao");
        ProfessoriDaoLocal p = (ProfessoriDaoLocal) pp;
        p.addProfessore(professore);
        p.getProfessore(1);

        Corsi corso = new Corsi();
        corso.setNome("nome");
        corso.setCfu(6);
        corso.setDescrizione("abc");
        corso.setIdProfessore(professore);
        corso.setId(0);
        Object cc = container.getContext().lookup("java:global/classes/CorsiDao");
        CorsiDaoLocal c = (CorsiDaoLocal) cc;
        c.addCorso(corso);
        c.getCorsiByNome("nome");
        c.deleteCorso(1);
        p.deleteProfessore(1);
    }

    /**
     * Test of deleteCorso method, of class CorsiDao.
     */
    @Test
    public void testDeleteCorso() throws Exception {
        System.out.println("deleteCorso");
        Professori professore = new Professori();
        professore.setCognome("cognome");
        professore.setNome("nome");
        professore.setRecapito("abc");
        professore.setId(0);
        Object pp = container.getContext().lookup("java:global/classes/ProfessoriDao");
        ProfessoriDaoLocal p = (ProfessoriDaoLocal) pp;
        p.addProfessore(professore);
        p.getProfessore(1);

        Corsi corso = new Corsi();
        corso.setNome("nome");
        corso.setCfu(6);
        corso.setDescrizione("abc");
        corso.setIdProfessore(professore);
        corso.setId(0);
        Object cc = container.getContext().lookup("java:global/classes/CorsiDao");
        CorsiDaoLocal c = (CorsiDaoLocal) cc;
        c.addCorso(corso);
        c.deleteCorso(1);
        p.deleteProfessore(1);
    }
}
