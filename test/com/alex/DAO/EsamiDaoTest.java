/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alex.DAO;

import com.alex.dao.StudentiDaoLocal;
import com.alex.model.Studenti;
import com.alex.dao.CorsiDaoLocal;
import com.alex.model.Corsi;
import com.alex.dao.EsamiDaoLocal;
import com.alex.model.Esami;
import com.alex.dao.ProfessoriDaoLocal;
import com.alex.model.Professori;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.embeddable.EJBContainer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Alex
 */
public class EsamiDaoTest {

    EJBContainer container;

    public EsamiDaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        Map<String, Object> properties = new HashMap<String, Object>();
        properties.put(EJBContainer.MODULES, new File("build\\web\\WEB-INF\\classes"));
        container = javax.ejb.embeddable.EJBContainer.createEJBContainer(properties);
    }

    @After
    public void tearDown() {
        container.close();
    }

    /**
     * Test of addEsame method, of class EsamiDao.
     */
    @Test
    public void testAddEsame() throws Exception {
        System.out.println("addEsame");
        Professori professore = new Professori();
        professore.setCognome("cognome");
        professore.setNome("nome");
        professore.setRecapito("abc");
        professore.setId(0);
        Object pp = container.getContext().lookup("java:global/classes/ProfessoriDao");
        ProfessoriDaoLocal p = (ProfessoriDaoLocal) pp;
        p.addProfessore(professore);
        p.getProfessore(1);

        Corsi corso = new Corsi();
        corso.setNome("nome");
        corso.setCfu(6);
        corso.setDescrizione("abc");
        corso.setIdProfessore(professore);
        corso.setId(0);
        Object cc = container.getContext().lookup("java:global/classes/CorsiDao");
        CorsiDaoLocal c = (CorsiDaoLocal) cc;
        c.addCorso(corso);
        c.getCorso(1);

        Studenti studente = new Studenti();
        studente.setCognome("cognome");
        studente.setNome("nome");
        studente.setMatricola(781061);
        studente.setAnnodicorso(2018);
        studente.setStudentiid(0);
        Object ss = container.getContext().lookup("java:global/classes/StudentiDao");
        StudentiDaoLocal s = (StudentiDaoLocal) ss;
        s.addStudente(studente);
        s.getStudente(1);

        Esami esame = new Esami();
        esame.setVoto(31);
        esame.setAnno(2018);
        esame.setIdCorso(corso);
        esame.setIdStudente(studente);
        esame.setId(0);
        Object ee = container.getContext().lookup("java:global/classes/EsamiDao");
        EsamiDaoLocal e = (EsamiDaoLocal) ee;
        e.addEsame(esame);

        e.deleteEsame(1);
        s.deleteStudente(1);
        c.deleteCorso(1);
        p.deleteProfessore(1);
    }

    /**
     * Test of editEsame method, of class EsamiDao.
     */
    @Test
    public void testEditEsame() throws Exception {
        System.out.println("editEsame");
        Professori professore = new Professori();
        professore.setCognome("cognome");
        professore.setNome("nome");
        professore.setRecapito("abc");
        professore.setId(0);
        Object pp = container.getContext().lookup("java:global/classes/ProfessoriDao");
        ProfessoriDaoLocal p = (ProfessoriDaoLocal) pp;
        p.addProfessore(professore);
        p.getProfessore(1);

        Corsi corso = new Corsi();
        corso.setNome("nome");
        corso.setCfu(6);
        corso.setDescrizione("abc");
        corso.setIdProfessore(professore);
        corso.setId(0);
        Object cc = container.getContext().lookup("java:global/classes/CorsiDao");
        CorsiDaoLocal c = (CorsiDaoLocal) cc;
        c.addCorso(corso);
        c.getCorso(1);

        Studenti studente = new Studenti();
        studente.setCognome("cognome");
        studente.setNome("nome");
        studente.setMatricola(781061);
        studente.setAnnodicorso(2018);
        studente.setStudentiid(0);
        Object ss = container.getContext().lookup("java:global/classes/StudentiDao");
        StudentiDaoLocal s = (StudentiDaoLocal) ss;
        s.addStudente(studente);
        s.getStudente(1);

        Esami esame = new Esami();
        esame.setVoto(31);
        esame.setAnno(2018);
        esame.setIdCorso(corso);
        esame.setIdStudente(studente);
        esame.setId(0);
        Object ee = container.getContext().lookup("java:global/classes/EsamiDao");
        EsamiDaoLocal e = (EsamiDaoLocal) ee;
        e.addEsame(esame);
        Esami result = e.getEsame(1);
        result.setVoto(30);
        e.editEsame(result);

        e.deleteEsame(1);
        s.deleteStudente(1);
        c.deleteCorso(1);
        p.deleteProfessore(1);
    }

    /**
     * Test of deleteEsame method, of class EsamiDao.
     */
    @Test
    public void testDeleteEsame() throws Exception {
        System.out.println("editEsame");
        Professori professore = new Professori();
        professore.setCognome("cognome");
        professore.setNome("nome");
        professore.setRecapito("abc");
        professore.setId(0);
        Object pp = container.getContext().lookup("java:global/classes/ProfessoriDao");
        ProfessoriDaoLocal p = (ProfessoriDaoLocal) pp;
        p.addProfessore(professore);
        p.getProfessore(1);

        Corsi corso = new Corsi();
        corso.setNome("nome");
        corso.setCfu(6);
        corso.setDescrizione("abc");
        corso.setIdProfessore(professore);
        corso.setId(0);
        Object cc = container.getContext().lookup("java:global/classes/CorsiDao");
        CorsiDaoLocal c = (CorsiDaoLocal) cc;
        c.addCorso(corso);
        c.getCorso(1);

        Studenti studente = new Studenti();
        studente.setCognome("cognome");
        studente.setNome("nome");
        studente.setMatricola(781061);
        studente.setAnnodicorso(2018);
        studente.setStudentiid(0);
        Object ss = container.getContext().lookup("java:global/classes/StudentiDao");
        StudentiDaoLocal s = (StudentiDaoLocal) ss;
        s.addStudente(studente);
        s.getStudente(1);

        Esami esame = new Esami();
        esame.setVoto(31);
        esame.setAnno(2018);
        esame.setIdCorso(corso);
        esame.setIdStudente(studente);
        esame.setId(0);
        Object ee = container.getContext().lookup("java:global/classes/EsamiDao");
        EsamiDaoLocal e = (EsamiDaoLocal) ee;
        e.addEsame(esame);

        e.deleteEsame(1);
        s.deleteStudente(1);
        c.deleteCorso(1);
        p.deleteProfessore(1);
    }
}
