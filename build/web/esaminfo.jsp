<%-- 
    Document   : esaminfo
    Created on : 7-dic-2018, 15.05.34
    Author     : Alex
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"/>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Informazioni Studente</title>
        <jsp:include page="header.jsp"/>
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><center><b><i>Informazioni Esami</i></b></center></div>
        <form action="./EsamiServlet" method="POST">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Campo</th>
                    </tr>
                </thead>
                <tbody>
                    <tr hidden="hidden">
                        <td>Esame ID</td>
                        <td><input class="form-control" type="hidden" name="id" value="${esame.id}" /></td>
                    </tr>
                    <tr>
                        <td>Voto</td>
                        <td><input class="form-control" type="number" name="voto" min="18" max="31" value="${esame.voto}" ${caricare==true?'required':''}/></td>
                    </tr>
                    <tr>
                        <td>Anno</td>
                        <td><input class="form-control" type="text" pattern="^(19|20)\d{2}$" name="anno" value="${esame.anno}" ${caricare==true?'required':''}/></td>
                    </tr>
                    <tr>
                        <td>Studente</td>
                        <td><select name="idStudente" class="form-control" ${caricare==true?'required':''}>
                                <option value="">Selezionare uno studente</option>
                                <c:forEach items="${allStudenti}" var="stud">
                                    <c:choose>
                                        <c:when test="${esame.idStudente.studentiid == stud.studentiid}">
                                            <option value="${stud.studentiid}" selected="selected">${stud.nome} ${stud.cognome}</option>    
                                        </c:when>
                                        <c:otherwise>
                                            <option value="${stud.studentiid}">${stud.nome} ${stud.cognome}</option>     
                                        </c:otherwise>
                                    </c:choose>            
                                </c:forEach>

                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Corso</td>
                        <td><select name="idCorso" class="form-control" ${caricare==true?'required':''}>
                                <option value="">Selezionare un corso</option>
                                <c:forEach items="${allCorsi}" var="stud">
                                    <c:choose>
                                        <c:when test="${esame.idCorso.id == stud.id}">
                                            <option value="${stud.id}" selected="selected">${stud.nome}</option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="${stud.id}">${stud.nome}</option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">                           
                            <button class="btn btn-default btn-lg" type="submit" name="action" value="Add">
                                <i class="fas fa-plus-circle"></i>
                            </button>
                            <c:if test="${esame.id!=null}">
                                <button class="btn btn-default btn-lg" type="submit" name="action" value="Edit">
                                    <i class="fas fa-pen-nib"></i>
                                </button> 
                            </c:if>
                            <input class="btn btn-default"  style="display:none;" type="submit" name="action" value="Load" id="caricaPagina" />
                        </td>                
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Studente</th>
                <th scope="col">Corso</th>
                <th scope="col">Voto</th>
                <th scope="col">Anno</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${allEsami}" var="stud">
                <tr>
                    <td>${stud.id}</td>
                    <td>${stud.idStudente.nome} ${stud.idStudente.cognome}</td>
                    <td>${stud.idCorso.nome}</td>
                    <td>${stud.voto}</td>
                    <td>${stud.anno}</td>
                    <td>
                        <form action="./EsamiServlet" method="POST">
                            <input type="hidden" name="id" value="${stud.id}"/>
                            <button class="btn btn-default" type="submit" name="action" value="Delete">
                                <i class="fas fa-trash-alt"></i>
                            </button>                        
                        </form>
                    </td>
                    <td>
                        <form action="./EsamiServlet" method="POST">
                            <input type="hidden" name="id" value="${stud.id}"/>
                            <button class="btn btn-default" type="submit" name="action" value="Load">
                                <i class="fas fa-upload"></i>
                            </button>
                        </form>
                    </td>
                </tr>
            </tbody>
        </c:forEach>
    </table> 
    <script>
        <c:if test="${caricare!=true}">
        $("#caricaPagina").trigger("click");
        </c:if>
    </script> 
</div>
</body>
</html>


