<%-- 
    Document   : index
    Created on : 7-dic-2018, 11.15.03
    Author     : Alex
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"/>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


        <title>JSP Home Page</title>
        <jsp:include page="header.jsp"/>

    <h1 class="panel-heading"><center><b><i>ASSIGNMENT 3</i></b></center></h1>
    <h3 class="panel-heading"><center><b><i>Processo e sviluppo software</i></b></center></h3>
    <h3 class="panel-heading"><center><b><i>Vazzola Alessandro 781061</i></b></center></h3>
    <br><br>
    <h4>The system consists of the following 4 entities: Courses - Exams - Professors - Students<br>
        You can "move" between the 4 possible screens and return to the home.<br>
        Within these screens, 4 operations (CRUD) + 1 (search) have been implemented:<br>
        <br>
        - <b>Add</b><br>
        - <b>Delete</b><br>
        - <b>Edit</b><br>
        - <b>Load</b><br>
        - <b>Search</b><br>
        <br>
        In the <i>STUDENTS</i> section the information of a student is displayed, with the possibility of assigning him a tutor.<br>
        Exams passed by a student are displayed in the <i>EXAMS</i> section.<br>
        In he <i>PROFESSORS</i> section the information of a professor is displayed.<br>
        In the <i>COURSES</i> section the courses that can be visited are displayed.<br>
    </h4>
</div>
</body>
</html>
